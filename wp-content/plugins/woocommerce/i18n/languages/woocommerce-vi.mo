��    6      �  I   |      �     �     �  @   �  -   �     *  F   ;     �     �  	   �  	   �     �     �  
   �     �     �     �            
   !  
   ,  V   7     �     �     �  	   �  	   �  /   �                     -     3     ?     G     P     m     ~     �     �     �     �     �     �       	         *  +   6     b     f     s     �  D   �     �  �  �     �
     �
  4   �
     �
     �
               (     5     D     S     j     }     �     �  
   �     �     �     �     �  [   �  -   I  0   w     �     �     �  V   �          +     9     E     U     b     o  ;   |     �     �     �  "        0     L  (   i  '   �     �     �     �  N   �     3     D     U  %   b  Y   �     �         4   	   &      '            ,      #          )          -   .              0       $   *                                        "   /      +   2      6             
            1   5   (                                               !      %      3        %d item %d items %s Items %s has been added to your cart. %s have been added to your cart. %s is a required field %s are required fields %s item %s items %s will be a number eventually, but must be a string for now.%s items Additional Information Address Address 1 Address 2 Billing Details Cart Totals Categories Company Name Default sorting Description Email Address Email address First Name First name I&rsquo;ve read and accept the <a href="%s" target="_blank">terms &amp; conditions</a> Invalid payment method. Invalid shipping method. Item Last Name Last name No products were found matching your selection. Order Notes Order Total Out of stock Phone Place order Product Products Ship to a different address? Shipping address Sort by average rating Sort by newness Sort by popularity Sort by price (asc) Sort by price (desc) Sort by price: high to low Sort by price: low to high Subtotal Subtotal: Town / City You must accept our Terms &amp; Conditions. ZIP ZIP/Postcode default-slugproduct is a required field. placeholderNotes about your order, e.g. special notes for delivery. product Project-Id-Version: WooCommerce 2.5.5
Report-Msgid-Bugs-To: https://github.com/woothemes/woocommerce/issues
POT-Creation-Date: 2016-03-11 17:54:31+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-10-18 15:17+0700
Last-Translator: admin <vicoders@gmail.com>
Language-Team: 
X-Generator: Poedit 1.8.10
Language: vi
Plural-Forms: nplurals=1; plural=0;
 %d Sản phẩm %s Sản phẩm %s đã được thêm vào giỏ hàng của bạn. %s là bắt buộc %s Sản phẩm %s Sản phẩm Thông tin thêm Địa chỉ Địa chỉ 1 Địa chỉ 2 Chi tiết thanh toán Tổng giỏ hàng Phân loại Tên công ty Sắp xếp mặc định Miêu tả Địa chỉ email Địa chỉ email Tên Tên Tôi đồng ý với <a href="%s" target="_blank">điều khoản &amp; điều kiện</a> Phương thức thanh toán không hợp lệ Phương thức vận chuyển không hợp lệ Sản phẩm Họ Họ Không có sản phẩm được tìm thấy phù hợp với lựa chọn của bạn Ghi chú đơn hàng Thành tiền Hết hàng Điện thoại Đặt hàng Sản phẩm Sản phẩm Vận chuyển tới một địa chỉ nhận hàng khác? Địa chỉ nhận hàng Sắp xếp theo đánh giá Sắp xếp theo mới nhất Sắp xếp theo mức yêu thích Sắp xếp theo giá (asc) Sắp xếp theo giá (desc) Thứ tự theo giá: cao xuống thấp Thứ tự theo giá: thấp đến cao Tổng cộng Tổng cộng: Thành phố Bạn phải chấp nhận Điều khoản & Điều kiện của chúng tôi. Mã bưu điện Mã bưu điện Sản phẩm Trường yêu cầu là bắt buộc Ghi chú về đặt hàng của bạn, ví dụ ghi chú đặc biệt để giao hàng. Sản phẩm 