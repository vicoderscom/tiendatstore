<?php
/**
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * Theme's functions.php file.
 *
 * This file bootstrap the entire framework.
 *
 * @package Yithemes
 *
 */


/*
 * WARNING: This file is part of the Your Inspiration Themes framework core.
 * Edit this section at your own risk.
 */

//let's start the game!
require_once('core/yit.php');
function wptuts_styles_with_the_lot()
{
    // Register the style like this for a theme:
    wp_register_style( 'custom-style', get_template_directory_uri() . '/asset/css/custom-style.css', array(), '20120208', 'all' );
 
    // For either a plugin or a theme, you can then enqueue the style:
    wp_enqueue_style( 'custom-style' );
}
add_action( 'wp_enqueue_scripts', 'wptuts_styles_with_the_lot' );

$footerColumn1 = array(
    'name' => __('Footer Column 1', 'footer-column-1'),
    'id' => 'footer-column-1',
    'description' => 'Footer Column 1',
    'class' => 'footer-column-1',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>'
    );
register_sidebar( $footerColumn1 );

$footerColumn2 = array(
    'name' => __('Footer Column 2', 'footer-column-2'),
    'id' => 'footer-column-2',
    'description' => 'Footer Column 2',
    'class' => 'footer-column-2',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>'
    );
register_sidebar( $footerColumn2 );

$footerColumn3 = array(
    'name' => __('Footer Column 3', 'footer-column-3'),
    'id' => 'footer-column-3',
    'description' => 'Footer Column 3',
    'class' => 'footer-column-3',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>'
    );
register_sidebar( $footerColumn3 );

$footerColumn4 = array(
    'name' => __('Footer Column 4', 'footer-column-4'),
    'id' => 'footer-column-4',
    'description' => 'Footer Column 4',
    'class' => 'footer-column-4',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>'
    );
register_sidebar( $footerColumn4 );