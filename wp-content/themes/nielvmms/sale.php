<?php
/*
Template Name: Sale
*/
?>
<?php get_header(); ?>
	<div class="container">
	<?php echo do_shortcode('[sale_products per_page="12"]'); ?>
	</div>
<?php get_footer(); ?>